# SPRING BOOT 

# JAVA 17
# Gradle 7.4.2
# Kotlin
# Database Postgres
- First make sure to create the database, with the following command:
- create database store;

# download dependencies
- gradle dependencies

# run project
- gradle bootRun