package com.kotlin.store.service

import com.kotlin.store.repository.RolRepository
import com.kotlin.store.repository.UserRepository
import com.kotlin.store.repository.entity.Rol
import com.kotlin.store.repository.entity.User
import com.kotlin.store.service.dto.UserDTO
import com.kotlin.store.service.mapper.UserMapper
import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers.any
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import java.util.Optional

class UserServiceTest {
    @InjectMocks
    private lateinit var userService: UserService

    @Mock
    private lateinit var userRepository: UserRepository

    @Mock
    private lateinit var rolRepository: RolRepository

    @Mock
    private lateinit var userMapper: UserMapper

    @BeforeEach
    fun setup() {
        MockitoAnnotations.openMocks(this)
    }

    @Test
    fun testDeleteUser() {
        val userIdEliminado = 123
        val userIdAEliminar = 123

        // Llama al método del servicioa probar
        userService.deleteUser(userIdAEliminar)

        // Verifica que se haya llamado al método del userRepository para eliminar el usuario con el ID especificado
        verify(userRepository).deleteById(userIdEliminado)
    }
}