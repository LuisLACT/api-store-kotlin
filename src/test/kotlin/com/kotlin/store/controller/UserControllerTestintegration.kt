package com.kotlin.store.controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.kotlin.store.service.UserService
import com.kotlin.store.service.dto.UserDTO
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import org.springdoc.core.converters.models.Pageable
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

@WebMvcTest(UserController::class)
class UserControllerTestintegration {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @MockBean
    private lateinit var userService: UserService

    val userDTO = UserDTO(
        2
        ,"Prueba 1"
        ,"123"
        ,"Usuario de prueba"
        ,"correo@ejemplo.com"
        , true
        , setOf("ADMIN"))

    @Test
    fun testAddUser() {

        Mockito.`when`(userService.addUser(ArgumentMatchers.any(UserDTO::class.java))).thenReturn(userDTO)

        mockMvc.perform(
            MockMvcRequestBuilders
            .post("/api/users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(userDTO))
        )
            .andExpect(MockMvcResultMatchers.status().isCreated())
            .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
    }

    @Test
    fun testGetAllUsers() {
        val userList: MutableList<UserDTO> = ArrayList()
        userList.add(userDTO)

        val userPage: Page<UserDTO> = PageImpl(userList)

        val pageNumber = 0
        val pageSize = 10
        val sortField = "nombre"
        val sortDirection = "asc"

//        val pageable: Pageable = PageRequest.of(
//            pageNumber, pageSize, Sort.by(Sort.Direction.valueOf(sortDirection), sortField)
//        )
//
//
//        Mockito.`when`(userService.getAllUsers(ArgumentMatchers.any(Pageable::class.java))).thenReturn(userPage)

        mockMvc.perform(
            MockMvcRequestBuilders
            .get("/api/users")
            .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
    }

    @Test
    fun testUpdateUser() {
        val userId = 1

        Mockito.`when`(userService.updateUser(Mockito.eq(userId), ArgumentMatchers.any(UserDTO::class.java))).thenReturn(userDTO)

        mockMvc.perform(
            MockMvcRequestBuilders
            .put("/api/users/{user_id}", userId)
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(userDTO))
        )
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
    }

    @Test
    fun testDeleteUser() {
        val userId = 2

        mockMvc.perform(
            MockMvcRequestBuilders
            .delete("/api/users/{user_id}", userId)
        )
            .andExpect(MockMvcResultMatchers.status().isNoContent())

        Mockito.verify(userService, Mockito.times(1)).deleteUser(userId)
    }
}