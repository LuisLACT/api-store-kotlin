package com.kotlin.store.controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.kotlin.store.controller.UserController
import com.kotlin.store.service.UserService
import com.kotlin.store.service.dto.UserDTO
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.mockito.ArgumentMatchers.any
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean

class UserControllerTest {

    private lateinit var mockMvc: MockMvc

    // Datos de prueba para UserDTO
    val userDTO = UserDTO(
        2
        ,"Prueba 1"
        ,"123"
        ,"Usuario de prueba"
        ,"correo@ejemplo.com"
        , true
        , setOf("ADMIN"))

    @InjectMocks
    private lateinit var userController: UserController

    @Mock
    private lateinit var userService: UserService

    @BeforeEach
    fun setup() {
        MockitoAnnotations.openMocks(this)

        // Configura el controlador y aplica las validaciones
        val validator = LocalValidatorFactoryBean()
        validator.afterPropertiesSet()

        mockMvc = MockMvcBuilders.standaloneSetup(userController)
            .setValidator(validator)
            .build()

    }

    @Test
    fun testAddUser() {
        // Simula el comportamiento del servicio mock (userService)
        `when`(userService.addUser(userDTO)).thenReturn(userDTO)

        // Realiza la solicitud POST al endpoint "/api/users" con el JSON de UserDTO
        mockMvc.perform(
            MockMvcRequestBuilders.post("/api/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ObjectMapper().writeValueAsString(userDTO))
        )
            .andExpect(MockMvcResultMatchers.status().isCreated)
            .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(2))
            .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Prueba 1"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.email").value("correo@ejemplo.com"))

        // Verifica que se haya llamado al método del servicio esperado
        verify(userService).addUser(userDTO)
    }

    @Test
    fun deleteUserTest() {
        val userIdToDelete = 10
        val userIdEsperado = 10

       // Llama al método deleteUser del controlador
        userController.deleteUser(userIdToDelete)

        // Verifica que el método deleteUser del servicio se haya llamado con el userId correcto
        verify(userService).deleteUser(userIdEsperado)

    }

    @Test
    fun getAllUsersTest_SuccessCode() {
        // Cofiguracion de los datos
        val pageable: Pageable = Pageable.ofSize(10)
        // Define un Page<UserDTO> de prueba
        val userDTOList = mutableListOf<UserDTO>()
        val pageUserDTO: Page<UserDTO> = PageImpl(userDTOList, pageable, userDTOList.size.toLong())

        // Configura el comportamiento del servicio mock (userService) para que devuelva la lista de usuarios
        `when`(userService.getAllUsers(pageable)).thenReturn(pageUserDTO)

        // Llama al método del controlador
        val responseEntity: ResponseEntity<Page<UserDTO>> = userController.getAllUsers(pageable)
        verify(userService).getAllUsers(pageable)

        assertThat(responseEntity.statusCode).isEqualTo(HttpStatus.OK)
//        assertThat(responseEntity.statusCode).isEqualTo(HttpStatus.CREATED) // Resultado esperado incorrecto
    }

}