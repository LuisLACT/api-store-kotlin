package com.kotlin.store.controller

import com.kotlin.store.security.jwt.JwtAuthTokenFilter
import com.kotlin.store.security.jwt.JwtUtils
import com.kotlin.store.vm.LoginVM
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean

class UserJwtControllerTest{

    @Mock
    private lateinit var authenticationManager: AuthenticationManager

    @Mock
    private lateinit var jwtUtils: JwtUtils

    @InjectMocks
    private lateinit var userJwtController: UserJwtController


    @BeforeEach
    fun setup() {
        MockitoAnnotations.openMocks(this)

        // Configura el controlador y aplica las validaciones
        val validator = LocalValidatorFactoryBean()
        validator.afterPropertiesSet()

    }

    @Test
    fun `authorize should return JWTToken and status Ok`() {
        val loginVM = LoginVM("maya", "123", rememberMe = true)
        val authentication: Authentication = UsernamePasswordAuthenticationToken(loginVM.username, loginVM.password)
        val jwt = "jwt"

        `when`(authenticationManager.authenticate(UsernamePasswordAuthenticationToken(loginVM.username, loginVM.password))).thenReturn(authentication)
        `when`(jwtUtils.generateJwtToken(authentication)).thenReturn(jwt)

        val responseEntity = userJwtController.authorize(loginVM)

        assert(responseEntity.statusCode == HttpStatus.OK)
//        assert(responseEntity.statusCode == HttpStatus.CREATED) // Resultado incorrecta
    }
}