package com.kotlin.store.controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.kotlin.store.vm.LoginVM
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@ExtendWith(SpringExtension::class)
@WebMvcTest(UserJwtController::class)
@AutoConfigureMockMvc
class UserJwtControllerTestIntegration {
    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var objectMapper: ObjectMapper


    @BeforeEach
    fun setUp() {
    }

    @Test
    fun testAuthenticateUser() {
        val loginVM = LoginVM("username", "password", true)

        mockMvc.perform(
            MockMvcRequestBuilders.post("/api/authenticate")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(loginVM)))
            .andExpect(status().isOk())

    }
}