package com.kotlin.store.controller

import com.kotlin.store.service.dto.CategoryDTO
import com.kotlin.store.service.CategoryService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
@RequestMapping("/api/categories")
@Validated
class CategoryController(private val categoryService: CategoryService) {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(CategoryController::class.java)
    }
    @GetMapping
    fun getAllCategory(pageable: Pageable) : ResponseEntity<Page<CategoryDTO>> {
        logger.debug("REST request to get all categories");
        val categories =  categoryService.getAllCategory(pageable)
        return ResponseEntity.ok(categories)
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun addCategory(@RequestBody @Valid categoryDTO: CategoryDTO) : CategoryDTO{
        logger.debug("REST request to add category");
        return categoryService.addCategory(categoryDTO)
    }


    @PutMapping("/{category_id}")
    fun updateCategory(@RequestBody categoryDTO: CategoryDTO,
                       @PathVariable("category_id") categoryId: Int): CategoryDTO {
        logger.debug("REST request to update category");
        return  categoryService.updateCategory(categoryDTO, categoryId)
    }


    @DeleteMapping("/{category_id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deleteCategory(@PathVariable("category_id") categoryId: Int) {
        logger.debug("REST request to delete category");
        categoryService.deleteCategory(categoryId)
    }
}
