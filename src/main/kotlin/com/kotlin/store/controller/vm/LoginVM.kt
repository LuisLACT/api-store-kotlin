package com.kotlin.store.vm

data class LoginVM (
    val username: String,
    val password: String,
    val rememberMe: Boolean
)
