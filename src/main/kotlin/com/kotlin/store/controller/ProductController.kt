package com.kotlin.store.controller

import com.kotlin.store.service.dto.ProductDTO
import com.kotlin.store.service.ProductService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
@RequestMapping("/api/product")
@Validated
class ProductController(private val productService: ProductService) {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(ProductController::class.java)
    }
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun addProduct(@RequestBody @Valid productDTO: ProductDTO) : ProductDTO {
        logger.debug("Request to save product")
        return productService.addProduct(productDTO)
    }

    @PutMapping("/{product_id}")
    fun updateProduct(@RequestBody productDTO: ProductDTO,
                      @PathVariable("product_id") productId: Int): ProductDTO {
        logger.debug("Request to update product")
        return productService.updateProduct(productDTO, productId)
    }

    @GetMapping
    fun getAllProduct(pageable: Pageable) : ResponseEntity<Page<ProductDTO>> {
        logger.debug("Request to get all products")
        val products = productService.getAllProduct(pageable)
        return ResponseEntity.ok().body(products)
    }

    @DeleteMapping("/{product_id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deleteProduct(@PathVariable("product_id") productId: Int) {
        logger.debug("Request to delete product")
        productService.deleteProduct(productId)
    }
}
