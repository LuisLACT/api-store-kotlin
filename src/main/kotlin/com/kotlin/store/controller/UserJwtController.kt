package com.kotlin.store.controller

import com.fasterxml.jackson.annotation.JsonProperty
import com.kotlin.store.vm.LoginVM
import com.kotlin.store.security.jwt.JwtAuthTokenFilter
import com.kotlin.store.security.jwt.JwtUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestBody

@CrossOrigin(origins = ["*"], maxAge = 3600)
@RestController
@RequestMapping("/api/authenticate")
class UserJwtController(private val authenticationManager: AuthenticationManager, private val jwtUtils: JwtUtils) {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(UserJwtController::class.java)
    }
    data class JWTToken(@JsonProperty("id_token") var idToken: String)

    @PostMapping
    fun authorize(@RequestBody loginVM: LoginVM): ResponseEntity<JWTToken> {
        logger.debug("Request to authenticate an user")
        val authentication = authenticationManager.authenticate(
            UsernamePasswordAuthenticationToken(loginVM.username, loginVM.password))
        SecurityContextHolder.getContext().authentication = authentication
        val jwt = jwtUtils.generateJwtToken(authentication)
        val httpHeaders = HttpHeaders()
        httpHeaders.add(JwtAuthTokenFilter.AUTHORIZATION_HEADER, "Bearer $jwt")
        return ResponseEntity(JWTToken(jwt), httpHeaders, HttpStatus.OK)
    }
}
