package com.kotlin.store.controller

import com.kotlin.store.service.CategoryService
import com.kotlin.store.service.EntranceService
import com.kotlin.store.service.dto.EntranceDTO
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import org.springframework.data.domain.Pageable
import org.springframework.http.ResponseEntity
import javax.validation.Valid
@RestController
@RequestMapping("/api/entrance")
@Validated
class EntranceController(private val entranceService: EntranceService) {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(EntranceController::class.java)
    }
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun registerEntrance(@RequestBody @Valid entranceDTO: EntranceDTO) : EntranceDTO {
        logger.debug("Request to register an entrance")
        return entranceService.registerEntrance(entranceDTO)
    }


    @GetMapping
    fun getAllEntrance(pageable: Pageable) : ResponseEntity<Page<EntranceDTO>> {
        logger.debug("Request to get all entrances")
       val entrances = entranceService.getAllEntrance(pageable)
        return ResponseEntity.ok().body(entrances)
    }

}

