package com.kotlin.store.controller

import com.kotlin.store.service.ProductFakeService
import com.kotlin.store.service.ProductFakeServiceImpl
import com.kotlin.store.service.ProductService
import com.kotlin.store.service.dto.ProductFakeDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import retrofit2.Call

@RestController
@RequestMapping("/api/product-fake")
class ProductFakeController {

    @Autowired
    private lateinit var productFakeServiceImpl: ProductFakeServiceImpl

    @GetMapping
    fun getProductsFake() : List<ProductFakeDto>? {
        return productFakeServiceImpl.getAllProducts()
    }

    @PostMapping
    fun saveProductsFake(@RequestBody productFakeDto: ProductFakeDto) : ProductFakeDto {
        return productFakeServiceImpl.saveProductFake(productFakeDto)!!
    }
}