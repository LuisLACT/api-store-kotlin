package com.kotlin.store.controller

import com.kotlin.store.service.dto.UserDTO
import com.kotlin.store.service.UserService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.PathVariable
import javax.validation.Valid

@CrossOrigin(origins = ["*"], maxAge = 3000)
@RestController
@RequestMapping("/api/users")
@Validated
class UserController(private val userService: UserService) {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(UserController::class.java)
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun addUser(@RequestBody @Valid userDTO: UserDTO) : UserDTO {
        logger.debug("Request to save user")
        return  userService.addUser(userDTO)
    }

    @GetMapping
    fun getAllUsers(pageable: Pageable) : ResponseEntity<Page<UserDTO>> {
        logger.debug("Request to get all user")
        val users = userService.getAllUsers(pageable)
        return ResponseEntity.ok().body(users)
    }

    @PutMapping("/{user_id}")
    fun updateUser(@RequestBody @Valid userDTO: UserDTO,
                   @PathVariable("user_id") userId: Int) : UserDTO {
        logger.debug("Request to update user")
        return userService.updateUser(userId, userDTO)
    }

    @DeleteMapping("/{user_id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deleteUser(@PathVariable("user_id") userId: Int) {
        logger.debug("Request to delete user")
        userService.deleteUser(userId)
    }
}
