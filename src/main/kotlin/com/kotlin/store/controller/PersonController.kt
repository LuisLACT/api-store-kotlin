package com.kotlin.store.controller

import com.kotlin.store.service.dto.PersonDTO
import com.kotlin.store.repository.entity.Person
import com.kotlin.store.service.PersonService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
@RequestMapping("/api/person")
@Validated
class PersonController(private val personService: PersonService) {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(PersonController::class.java)
    }
    @GetMapping
    fun getAllPerson(pageable: Pageable) : ResponseEntity<Page<PersonDTO>> {
        logger.debug("Request to get all people")
        val person = personService.getAllPerson(pageable)
        return ResponseEntity.ok().body(person)
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun addPerson(@RequestBody @Valid personDTO: PersonDTO) : PersonDTO {
        logger.debug("Request to save person")
        return personService.addPerson(personDTO)
    }

    @PutMapping("/{person_id}")
    fun updatePerson(@RequestBody @Valid personDTO: PersonDTO,
                     @PathVariable("person_id") personId: Int) : PersonDTO {
        logger.debug("Request to update person")
        return personService.updatePerson(personDTO, personId)
    }


    @DeleteMapping("/{person_id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deletePerson( @PathVariable("person_id") personId: Int) {
        logger.debug("Request to delete person")
        personService.deletePerson(personId)
    }
}
