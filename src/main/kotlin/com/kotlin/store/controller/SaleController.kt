package com.kotlin.store.controller

import com.kotlin.store.service.dto.SaleDTO
import com.kotlin.store.service.SaleService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
@RequestMapping("/api/sale")
@Validated
class SaleController(private val saleService: SaleService) {
    companion object {
        val logger: Logger = LoggerFactory.getLogger(SaleController::class.java)
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun makeSale(@RequestBody @Valid saleDTO: SaleDTO) : SaleDTO {
        logger.debug("Request to register sale")
        return saleService.registerSale(saleDTO)
    }

    @GetMapping
    fun getAllSale(pageable: Pageable): ResponseEntity<Page<SaleDTO>> {
        logger.debug("Request to get all sales")
        val sales = saleService.getAllSale(pageable)
        return ResponseEntity.ok().body(sales)
    }

}
