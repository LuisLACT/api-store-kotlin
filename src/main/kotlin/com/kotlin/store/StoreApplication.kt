package com.kotlin.store

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.openfeign.EnableFeignClients
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.web.servlet.config.annotation.EnableWebMvc


@SpringBootApplication
@EnableWebMvc
@EnableFeignClients
class StoreApplication

fun main(args: Array<String>) {
	val passwordEncoder = BCryptPasswordEncoder()
	val userpassword = passwordEncoder.encode("admin") as String
	println("========== PASSWORD $userpassword")
	runApplication<StoreApplication>(*args)
}
