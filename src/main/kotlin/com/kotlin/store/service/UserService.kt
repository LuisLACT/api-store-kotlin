package com.kotlin.store.service

import com.kotlin.store.service.dto.UserDTO
import com.kotlin.store.repository.entity.Rol
import com.kotlin.store.repository.RolRepository
import com.kotlin.store.repository.UserRepository
import com.kotlin.store.service.mapper.UserMapper
import com.kotlin.store.service.utils.Validations
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service


@Service
class UserService(
    private val userRepository: UserRepository,
    private val rolRepository: RolRepository,
    private val userMapper: UserMapper) {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(UserService::class.java)
    }

    fun addUser(userDTO: UserDTO) : UserDTO {
        logger.debug("Service to save user: {}", userDTO)
        val passwordEncoder = BCryptPasswordEncoder()
        userDTO.password =  passwordEncoder.encode(userDTO.password) as String
        val verifyUser = userRepository.findOneByEmailOrUsername(userDTO.email, userDTO.username)
        Validations.isUsernameAndEmailAvailable(verifyUser, userDTO.username, userDTO.email)
        userDTO.rol.map {
            Validations.validationRol(rolRepository.findById(it))
        }
        val userEntityMapper = userMapper.toEntity(userDTO)
        val userEntity = userRepository.save(userEntityMapper)
        return userMapper.toDTO(userEntity)
    }

    fun getAllUsers(pageable: Pageable) : Page<UserDTO> {
        logger.debug("Service to get all users")
        return userRepository
            .findAll(pageable)
            .map { userMapper.toDTO(it) }
    }

    fun updateUser(userId: Int, userDTO: UserDTO) : UserDTO {
        logger.debug("Service to update user: {}", userDTO)
        val passwordEncoder = BCryptPasswordEncoder()
        val userPassword = passwordEncoder.encode(userDTO.password) as String
        val userEntity = userRepository.findById(userId)
        Validations.validationUser(userEntity)
        return userEntity.get().let {
                it.name = userDTO.name
                it.password = userPassword
                userRepository.save(it)
                userMapper.toDTO(it)
        }
    }

    fun deleteUser(userId: Int) {
        logger.debug("Service to delete user with ID: {}", userId)
        userRepository.deleteById(userId)
    }
}
