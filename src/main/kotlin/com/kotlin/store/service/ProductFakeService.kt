package com.kotlin.store.service

import com.kotlin.store.service.dto.ProductFakeDto
import org.springframework.stereotype.Component
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface ProductFakeService {

    @GET("/api/v1/products")
    fun getAllProducts() : Call<List<ProductFakeDto>>?

    @POST("/api/v1/products")
    fun saveProductFake(@Body productFakeDto: ProductFakeDto) : Call<ProductFakeDto>
}