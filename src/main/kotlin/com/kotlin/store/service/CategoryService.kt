package com.kotlin.store.service

import com.kotlin.store.service.dto.CategoryDTO
import com.kotlin.store.repository.CategoryRepository
import com.kotlin.store.service.utils.Validations
import com.kotlin.store.service.mapper.CategoryMapper
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

@Service
class CategoryService(
    val categoryRepository: CategoryRepository,
    val categoryMapper: CategoryMapper
) {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(CategoryService::class.java)
    }
    fun addCategory(categoryDTO: CategoryDTO) : CategoryDTO {
        logger.debug("Service to save category : {}", categoryDTO)
        val category = categoryRepository.save(categoryMapper.toEntity(categoryDTO))
        return categoryMapper.toDTO(category)
    }

    fun updateCategory(categoryDTO: CategoryDTO, categoryId: Int) : CategoryDTO {
        logger.debug("Service to update category : {}", categoryDTO)
        val categoryEntity = categoryRepository.findById(categoryId)
        Validations.validationCategory(categoryEntity)
        return categoryEntity.get().let {
            it.name = categoryDTO.name
            it.description = categoryDTO.description
            categoryRepository.save(it)
            categoryMapper.toDTO(it)
        }
    }

    fun deleteCategory(categoryId: Int) {
        logger.debug("Service to delete category with id: {}", categoryId)
        val categoryEntity = categoryRepository.findById(categoryId)
        Validations.validationCategory(categoryEntity)
        categoryRepository.deleteById(categoryId)
    }

    fun getAllCategory(pageable: Pageable) : Page<CategoryDTO> {
        logger.debug("Service to get all categories")
        val categoryList = categoryRepository.findAll(pageable)
        return categoryList.map {
            categoryMapper.toDTO(it)
        }
    }
}