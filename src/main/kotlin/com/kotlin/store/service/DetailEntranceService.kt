package com.kotlin.store.service

import com.kotlin.store.exceptions.CustomNotFoundException
import com.kotlin.store.repository.DetailEntranceRepository
import com.kotlin.store.repository.entity.DetailEntrance
import com.kotlin.store.repository.entity.Entrance
import com.kotlin.store.service.dto.DetailEntranceDTO
import com.kotlin.store.service.mapper.DetailEntranceMapper
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class DetailEntranceService(
    val detailEntranceRepository: DetailEntranceRepository,
    val productService: ProductService,
    val detailEntranceMapper: DetailEntranceMapper
) {
    companion object {
        val logger: Logger = LoggerFactory.getLogger(DetailEntranceService::class.java)
    }
    fun addDetailEntranceList(detailEntranceDTOList: MutableList<DetailEntranceDTO>,
                              entrance: Entrance) : MutableList<DetailEntranceDTO>{
        logger.debug("Service to add detailEntranceList by entrance")
        val listDetailEntranceDTO : MutableList<DetailEntranceDTO> = ArrayList()
        detailEntranceDTOList.map { detailEntranceDTO ->
            val product = productService.updateStockProduct(
                detailEntranceDTO.productId?: throw CustomNotFoundException("Product is null"),
                detailEntranceDTO.quantity,
                !entrance.status)
            val detailEntrance = detailEntranceMapper.toEntity(detailEntranceDTO, entrance, product)
            detailEntranceRepository.save(detailEntrance)
            listDetailEntranceDTO.add(
                detailEntranceMapper.toDTO(detailEntrance)
            )
        }
        return listDetailEntranceDTO
    }

    fun getAllDetailEntranceByEntranceId(entranceId : Int) : List<DetailEntrance> {
        logger.debug("Service to get all DetailEntranceList by Entrance ID")
        return detailEntranceRepository
            .findAllByEntranceId(entranceId)
    }
}
