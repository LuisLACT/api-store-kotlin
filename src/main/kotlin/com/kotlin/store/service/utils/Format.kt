package com.kotlin.store.service.utils

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

object Format {

    fun formatDate(localDateTime: LocalDateTime): String {
        val formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss")
        return localDateTime.format(formatter)
    }
}