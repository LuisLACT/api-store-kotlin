package com.kotlin.store.service.utils.anotations
import com.kotlin.store.service.utils.DecimalMaxDigitsValidator
import javax.validation.Constraint
import kotlin.reflect.KClass

@Target(AnnotationTarget.FIELD, AnnotationTarget.PROPERTY_GETTER)
@Retention(AnnotationRetention.RUNTIME)
@Constraint(validatedBy = [DecimalMaxDigitsValidator::class])
annotation class DecimalMaxDigits(
    val message: String = "Invalid decimal format",
    val groups: Array<KClass<out Any>> = [],
    val payload: Array<KClass<out Any>> = []
)
