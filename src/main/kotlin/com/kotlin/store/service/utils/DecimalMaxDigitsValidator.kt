package com.kotlin.store.service.utils

import com.kotlin.store.service.utils.anotations.DecimalMaxDigits
import java.math.BigDecimal
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext
import kotlin.reflect.KClass
import kotlin.annotation.AnnotationTarget

class DecimalMaxDigitsValidator: ConstraintValidator<DecimalMaxDigits, BigDecimal>{

    override fun isValid(value: BigDecimal?, context: ConstraintValidatorContext): Boolean {
        if (value == null) {
            return true
        }
        val maxDecimalDigits = 2
        val decimalPart = value.stripTrailingZeros().scale()
        return decimalPart <= maxDecimalDigits
    }
}
