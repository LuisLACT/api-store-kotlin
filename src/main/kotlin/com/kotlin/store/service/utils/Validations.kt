package com.kotlin.store.service.utils

import com.kotlin.store.controller.CategoryController
import com.kotlin.store.repository.entity.Rol
import com.kotlin.store.repository.entity.Category
import com.kotlin.store.repository.entity.Person
import com.kotlin.store.repository.entity.Product
import com.kotlin.store.repository.entity.Sale
import com.kotlin.store.repository.entity.User
import com.kotlin.store.exceptions.CustomNotFoundException
import com.kotlin.store.service.SaleService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.math.BigDecimal
import java.time.Duration
import java.time.LocalDateTime
import java.util.*

object Validations {

        val logger: Logger = LoggerFactory.getLogger(Validations::class.java)

        fun validationRol( rol: Optional<Rol>) {
            if(!rol.isPresent) {
                logger.error("Rol ${rol.get().name} not found")
                throw CustomNotFoundException("Rol ${rol.get().name} not found")
            }
        }

        fun validationPerson(client: Optional<Person>) {
            if(!client.isPresent) {
                logger.error("Person with id: ${client.get().id} not found")
                throw CustomNotFoundException("Person with id: ${client.get().id} not found")
            }
        }

        fun validationUser(user : Optional<User>){
            if(!user.isPresent) {
                logger.error("User with id: ${user.get().id} not found")
                throw CustomNotFoundException("User with id: ${user.get().id} not found")
            }
        }

        fun isUsernameAndEmailAvailable(user: Optional<User>, username: String, email: String) {
            if(user.isPresent) {
                if(user.get().username == username || user.get().email == email) {
                    logger.error("Username ${user.get().username} or email: ${user.get().email} not available")
                    throw CustomNotFoundException("""
                        Username ${user.get().username} or email: ${user.get().email} not available
                    """.trimIndent())
                }
            }
        }

        fun validateTime(sale: Sale) : Boolean{
            val duration = Duration.between(LocalDateTime.now(), sale.date)
            if(kotlin.math.abs(duration.toMinutes()) <= SaleService.MINUTES) {
                return true
            }
            return false
        }

        fun validationProduct(product: Optional<Product>) {
            if (!product.isPresent) {
                logger.error("Product with id: ${product.get().id} not found")
                throw CustomNotFoundException("Product with id: ${product.get().id} not found")
            }
        }

        fun validationCategory(category : Optional<Category>) {
            if(!category.isPresent) {
                logger.error("Category with id: ${category.get().id} not found")
                throw CustomNotFoundException("Category with id: ${category.get().id} not found")
            }
        }

        fun isProductAvailable(product: Product, quantity: BigDecimal) {
            if(quantity > product.stock) {
                logger.error("Product with id: ${product.id} not available")
                throw  CustomNotFoundException("Product with id: ${product.id} not available")
            }
        }
}
