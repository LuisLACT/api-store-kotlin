package com.kotlin.store.service.dto

import com.kotlin.store.repository.entity.enumeration.PersonType
import javax.validation.constraints.Digits
import javax.validation.constraints.Email
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size

data class PersonDTO(
    val id: Int?,
    val personType: PersonType,
    @get: NotBlank(message = "Person: name not be blank")
    @get: Size(min=3, max = 50, message = "Person: name length must be between 3 and 30 characters")
    @get: Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]+$", message = "Person: name disallowed characters")
    val name: String,
    @get: NotBlank(message = "Person: document type not be blank")
    @get: Size(min=2, max = 30, message = "Person: document type length must be between 2 and 30 characters")
    @get: Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ]+$", message = "Person: document type disallowed characters")
    val documentType: String,
    @get:Digits(integer = 10, fraction = 0, message = "Person: phone disallowed characters")
    val documentNumber: Int,
    @get: Size(max = 100, message = "Person: address length must be up to 100 characters")
    @get: Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ0-9#?\\-,. ]+$", message = "Person: address disallowed characters")
    val address: String,
    @get:Digits(integer = 10, fraction = 0, message = "Person: phone disallowed characters")
    val phone: Int,
    @get: Email(message = "Person: email format incorrect ")
    val email: String
)

