package com.kotlin.store.service.dto

data class ProductFakeDto(
    val id :Int?,
    val title: String,
    val price : Double,
    val description: String,
    val categoryId: Int,
    var images: MutableList<String>?
)
