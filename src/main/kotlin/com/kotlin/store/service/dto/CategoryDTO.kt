package com.kotlin.store.service.dto

import javax.validation.constraints.NotBlank
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size



data class CategoryDTO(

    val id: Int?,
    @get: NotBlank(message = "Category: name not be blank")
    @get: Size(min=3, max = 30, message = "Category: name length must be between 3 and 30 characters")
    @get: Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ0-9]+$", message = "Category: name disallowed characters")
    val name: String,
    @get: Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ0-9]+$", message = "Category: description disallowed characters")
    val description: String
)
