package com.kotlin.store.service.dto

import javax.validation.constraints.Email
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size

data class UserDTO (
    val id: Int?,
    @get: NotBlank(message = "User: name not be blank")
    @get: Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ0-9 ]+\$", message = "User: name must contain only letters and spaces")
    @get: Size(min = 3, max = 30, message = "User: name length must be between 3 and 30 characters")
    val name: String,
    @get: NotBlank(message = "User: password not be blank")
    @get: Size(min = 3, max = 30, message = "User: password length must be between 3 and 30 characters")
    var password: String?,
    @get: NotBlank(message = "User: username not be blank")
    @get: Size(min = 3, max = 30, message = "User: username length must be between 3 and 30 characters")
    val username: String,
    @get: NotBlank(message = "User: email not be blank")
    @get: Email(message = "User: email format incorrect ")
    val email: String,
    val status: Boolean,
    val rol: Set<String>
)
