package com.kotlin.store.service.dto

import com.kotlin.store.service.utils.anotations.DecimalMaxDigits
import java.math.BigDecimal
import javax.validation.constraints.Digits
import javax.validation.constraints.NotNull

data class DetailSaleDTO(
    val id: Int?,
    @get: NotNull(message = "Quantity not be null")
    @get: Digits(integer = 10, fraction = 0, message = "Quantity must be up to 10 digits")
    val quantity: BigDecimal,
    @get: NotNull(message = "Price not be null")
    @get: DecimalMaxDigits(message = "Price must be up to 2 decimals")
    val price: BigDecimal,
    @get: DecimalMaxDigits(message = "Discount must be up to 2 decimals")
    val discount: BigDecimal,
    val productId: Int?,
    val saleId: Int?
)

