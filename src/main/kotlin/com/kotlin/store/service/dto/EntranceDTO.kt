package com.kotlin.store.service.dto

import com.kotlin.store.service.utils.anotations.DecimalMaxDigits
import java.math.BigDecimal
import java.time.LocalDateTime
import javax.validation.constraints.Pattern
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size
import javax.validation.constraints.Digits
import javax.validation.constraints.NotNull

data class EntranceDTO(
    val id: Int?,
    @get: NotBlank(message = "Entrance: voucherType not be blank")
    @get: Size(min=1, max = 30, message = "Entrance: voucherType length must be between 1 and 30 characters")
    @get: Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ0-9\\- ]+$", message = "Entrance: voucherType disallowed characters")
    val  voucherType: String,
    @get: NotBlank(message = "Entrance: voucherSeries not be blank")
    @get: Size(min=1, max = 30, message = "Entrance: voucherSeries length must be between 1 and 30 characters")
    @get: Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ0-9\\- ]+$", message = "Entrance: voucherSeries disallowed characters")
    val voucherSeries: String,
    @get:Digits(integer = 10, fraction = 0, message = "Entrance: voucherNumber must be between 1 and 10 integers")
    val voucherNumber: Int,
    var date: String?,
    @get: NotNull(message = "Entrance: total not be null")
    @get: DecimalMaxDigits(message = "Entrance: total invalid decimal format")
    val total: BigDecimal,
    val status: Boolean,
    val personId: Int?,
    val userId: Int?,
    val providerName: String?,
    val providerDocumentNumber: Int?,
    val userNick: String?,
    val listDetailEntranceDTO: MutableList<DetailEntranceDTO>?
)
