package com.kotlin.store.service.dto

import com.kotlin.store.service.utils.anotations.DecimalMaxDigits
import java.math.BigDecimal
import java.time.LocalDateTime
import javax.validation.Valid
import javax.validation.constraints.Digits
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size
import javax.validation.constraints.NotNull
data class SaleDTO (
    val id: Int?,
    @get: NotBlank(message = "Sale: serie not be blank")
    @get: Size(min=1, max = 30, message = "Sale: serie length must be between 1 and 30 characters")
    @get: Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ0-9\\- ]+$", message = "Sale: serie disallowed characters")
    val serie: String,
    @get:Digits(integer = 10, fraction = 0, message = "Sale: voucherNumber must be between 1 and 10 integers")
    val voucherNumber: Int,
    var date: String?,
    @get: NotNull(message = "Sale: total not be null")
    @get: DecimalMaxDigits(message = "Sale: total invalid decimal format")
    val total: BigDecimal,
    val status: Boolean,
    val personId:Int?,
    val userId: Int?,
    val clientName: String?,
    val clientNumberDocument: Int?,
    val userNick: String?,
    @field:Valid
    val listDetailSaleDTO: MutableList<DetailSaleDTO>?
)
