package com.kotlin.store.service.dto

import com.kotlin.store.service.utils.anotations.DecimalMaxDigits
import java.math.BigDecimal
import javax.validation.constraints.Digits
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size
import javax.validation.constraints.NotNull

data class ProductDTO(
    val id: Int?,
    @get: NotBlank(message = "Product: code not be blank")
    @get: Size(min=1, max = 30, message = "Product: code length must be between 1 and 30 characters")
    @get: Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ0-9\\- ]+$", message = "Product: code disallowed characters")
    val code: String,
    @get: NotBlank(message = "Product: name not be blank")
    @get: Size(min=2, max = 30, message = "Product: name length must be between 2 and 30 characters")
    @get: Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ0-9 ]+$", message = "Product: name disallowed characters")
    val name: String,
    @get: NotNull(message = "Product: priceSale not be null")
    @get: DecimalMaxDigits(message = "Product: priceSale invalid decimal format")
    val priceSale: BigDecimal,
    @get: NotNull(message = "Product: stock not be blank")
    @get:Digits(integer = 10, fraction = 0, message = "Product: stock disallowed characters")
    val stock: BigDecimal,
    @get: Size(max = 100, message = "Product: description length must be up to 100 characters")
    @get: Pattern(regexp = "^[a-zA-ZáéíóúÁÉÍÓÚñÑ0-9 ]+$", message = "Product: description disallowed characters")
    val description: String,
    val status: Boolean,
    val categoryId: Int?
)
