package com.kotlin.store.service

import com.kotlin.store.exceptions.CustomNotFoundException
import com.kotlin.store.repository.PersonRepository
import com.kotlin.store.repository.UserRepository
import com.kotlin.store.repository.SaleRepository
import com.kotlin.store.service.dto.DetailSaleDTO
import com.kotlin.store.service.dto.SaleDTO
import com.kotlin.store.service.mapper.DetailSaleMapper
import com.kotlin.store.service.mapper.SaleMapper
import com.kotlin.store.service.utils.Format
import com.kotlin.store.service.utils.Validations
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import java.time.LocalDateTime

@Service
class SaleService(
    private val detailSaleService: DetailSaleService,
    private val personRepository: PersonRepository,
    private val userRepository: UserRepository,
    private val saleRepository: SaleRepository,
    private val saleMapper: SaleMapper,
    private val detailSaleMapper: DetailSaleMapper) {

    companion object {
        const val MINUTES = 1440
        val logger: Logger = LoggerFactory.getLogger(SaleService::class.java)
    }

    fun registerSale(saleDTO: SaleDTO) : SaleDTO{
        logger.debug("Service to register sale: {}", saleDTO)
        val client = personRepository.findById(saleDTO.personId?: throw CustomNotFoundException("Person not found"))
        Validations.validationPerson(client)
        val  user = userRepository.findById(saleDTO.userId?: throw CustomNotFoundException("User not found"))
        Validations.validationUser(user)
        val saleEntity = saleMapper.toEntity(saleDTO, client.get(), user.get())
        saleEntity.date = LocalDateTime.now()
        saleRepository.save(saleEntity)
        val listDetailSaleDTO = detailSaleService.addDetailSaleList(
            saleDTO.listDetailSaleDTO?: throw CustomNotFoundException("Sale not found"),
            saleEntity)
        return saleMapper.toDTO(saleEntity, listDetailSaleDTO)
    }

    fun getAllSale(pageable: Pageable): Page<SaleDTO> {
        logger.debug("Service to get all sale")
        val saleList = saleRepository.findAll(pageable)
        val saleListDTO: MutableList<SaleDTO> = ArrayList()
        saleList.map { sale ->
            val detailSaleListDTO : MutableList<DetailSaleDTO> = ArrayList()
            val detailSaleList = detailSaleService
                .findAllDetailSaleBySaleId(sale.id?: throw CustomNotFoundException("Sale not found"))
            detailSaleList.map { detailSale ->
                detailSaleListDTO.add(detailSaleMapper.toDTO(detailSale))
            }
            val saleDTO = saleMapper.toDTO(sale, detailSaleListDTO)
            saleDTO.date = Format.formatDate(sale.date!!)
            saleListDTO.add(saleDTO)
        }
        return PageImpl(saleListDTO)
    }
}
