package com.kotlin.store.service

import com.kotlin.store.exceptions.CustomNotFoundException
import com.kotlin.store.service.dto.ProductDTO
import com.kotlin.store.repository.entity.Product
import com.kotlin.store.repository.CategoryRepository
import com.kotlin.store.repository.ProductRepository
import com.kotlin.store.service.mapper.ProductMapper
import com.kotlin.store.service.utils.Validations
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service
import org.springframework.data.domain.Pageable
import java.math.BigDecimal

@Service
class ProductService(
    private val productRepository: ProductRepository,
    private val productMapper: ProductMapper,
    private val categoryRepository: CategoryRepository,
    private val productFakeServiceImpl: ProductFakeServiceImpl
) {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(ProductService::class.java)
    }

    fun addProduct(productDTO: ProductDTO) : ProductDTO {
        logger.debug("Service to save product: {}", productDTO)
        val categoryEntity = categoryRepository
            .findById(productDTO.categoryId?: throw CustomNotFoundException("Category not found"))
        Validations.validationCategory(categoryEntity)
        val productEntity = productRepository.save(productMapper.toEntity(productDTO))
        val productFakeDto = productMapper.toProductFakeDto(productDTO)
        productFakeDto.images = mutableListOf("https://placeimg.com/640/480/any")
        productFakeServiceImpl.saveProductFake(productFakeDto)
        return productMapper.toDTO(productEntity)
    }

    fun updateProduct(productDTO: ProductDTO, productId : Int) : ProductDTO {
        logger.debug("Service to update product: {}", productDTO)
        val productEntity = productRepository.findById(productId)
        Validations.validationProduct(productEntity)
        val category = categoryRepository
            .findById(productDTO.categoryId?: throw CustomNotFoundException("Category not found"))
        Validations.validationCategory(category)

        return productEntity.get().let {
            it.code = productDTO.code
            it.name = productDTO.name
            it.priceSale = productDTO.priceSale
            it.stock = productDTO.stock
            it.status = productDTO.status
            it.description = productDTO.description
            it.category = category.get()
            productRepository.save(it)
            productMapper.toDTO(it)
        }
    }

    fun getAllProduct(pageable: Pageable): Page<ProductDTO> {
        logger.debug("Service to get all products")
        return productRepository
            .findAll(pageable)
            .map { productMapper.toDTO(it) }
    }

    fun deleteProduct(productId: Int) {
        logger.debug("Service to delete product with ID: {}", productId)
        val productEntity = productRepository.findById(productId)
        Validations.validationProduct(productEntity)
        productRepository.deleteById(productId)
    }

    fun updateStockProduct(id: Int, quantity: BigDecimal, status: Boolean) : Product {
        logger.debug("Service to update stock product")
        val product = productRepository.findById(id)
        Validations.validationProduct(product)
        Validations.isProductAvailable(product.get(), quantity)
        product.get().stock = if(status)
        {
            product.get().stock - quantity
        } else {
            product.get().stock + quantity
        }
        productRepository.save(product.get())
        return product.get()
    }
}
