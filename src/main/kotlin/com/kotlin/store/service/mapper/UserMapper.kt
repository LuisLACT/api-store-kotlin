package com.kotlin.store.service.mapper

import com.kotlin.store.repository.entity.Rol
import com.kotlin.store.service.dto.UserDTO
import com.kotlin.store.repository.entity.User
import org.mapstruct.Mapper
import org.mapstruct.Mapping

@Mapper(componentModel = "spring")
abstract class UserMapper {
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "password", source = "userDTO.password", defaultValue = "defaultPassword")
    abstract fun toEntity(userDTO: UserDTO): User

    @Mapping(target = "rol", expression = "java(mapRoles(user.getRol()))")
    abstract fun toDTO(user: User): UserDTO

    fun mapRoles(roles: Set<Rol>): Set<String> {
        return roles.map { it.name }.toSet()
    }

    fun mapRolesFromString(roleNames: Set<String>): Set<Rol> {
        return roleNames.map { roleName -> Rol(roleName) }.toSet()
    }
}