package com.kotlin.store.service.mapper

interface EntityMapper<D, E> {
    fun toEntity(dto: D): E
    fun toDTO(entity: E): D
}