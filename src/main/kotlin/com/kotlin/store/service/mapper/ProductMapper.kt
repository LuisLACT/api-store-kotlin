package com.kotlin.store.service.mapper

import com.kotlin.store.repository.entity.Product
import com.kotlin.store.service.dto.ProductDTO
import com.kotlin.store.service.dto.ProductFakeDto
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.ValueMapping

import org.mapstruct.ValueMappings




@Mapper(componentModel = "spring", uses = [CategoryMapper::class])
interface ProductMapper : EntityMapper<ProductDTO, Product>{

    @Mapping(source = "category.id", target = "categoryId")
    override fun toDTO(entity: Product): ProductDTO

    @Mapping(source = "dto.categoryId", target = "category.id")
    override fun toEntity(dto: ProductDTO): Product

    @Mapping(source = "dto.name", target = "title")
    @Mapping(source = "dto.priceSale", target = "price")
    @Mapping(source = "dto.description", target = "description")
    @Mapping(source = "dto.categoryId", target = "categoryId")
    @Mapping(target = "images", ignore = true)
    fun toProductFakeDto(dto: ProductDTO) : ProductFakeDto

}