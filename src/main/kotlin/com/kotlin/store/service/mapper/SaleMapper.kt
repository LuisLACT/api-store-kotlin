package com.kotlin.store.service.mapper

import com.kotlin.store.repository.entity.Person
import com.kotlin.store.repository.entity.User
import com.kotlin.store.repository.entity.Sale
import com.kotlin.store.service.dto.DetailSaleDTO
import com.kotlin.store.service.dto.SaleDTO
import org.mapstruct.Mapper
import org.mapstruct.Mapping

@Mapper(componentModel = "spring")
interface SaleMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(source = "saleDTO.status", target = "status")
    @Mapping(source = "user", target = "user")
    @Mapping(source = "person", target = "person")
    fun toEntity(saleDTO: SaleDTO, person: Person, user: User) : Sale

    @Mapping(source = "listDetailSaleDTO", target = "listDetailSaleDTO")
    @Mapping(source = "sale.person.name", target = "clientName")
    @Mapping(source = "sale.person.documentNumber", target = "clientNumberDocument")
    @Mapping(source = "sale.user.username", target = "userNick")
    @Mapping(target="personId", ignore = true)
    @Mapping(target="userId", ignore = true)
    fun toDTO(sale: Sale, listDetailSaleDTO: MutableList<DetailSaleDTO>) : SaleDTO

}
