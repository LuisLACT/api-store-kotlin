package com.kotlin.store.service.mapper

import com.kotlin.store.repository.entity.Entrance
import com.kotlin.store.repository.entity.Person
import com.kotlin.store.repository.entity.User
import com.kotlin.store.service.dto.DetailEntranceDTO
import com.kotlin.store.service.dto.EntranceDTO
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.springframework.stereotype.Service

@Mapper(componentModel = "spring", uses = [UserMapper::class, PersonMapper::class])
interface EntranceMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(source = "person", target = "person")
    @Mapping(source = "user", target = "user")
    @Mapping(source = "entrance.status", target = "status")
    fun toEntity(entrance: EntranceDTO, user: User, person: Person) : Entrance

    @Mapping(source = "entrance.person.name", target = "providerName")
    @Mapping(source = "entrance.person.documentNumber", target = "providerDocumentNumber")
    @Mapping(source = "entrance.user.username", target = "userNick")
    @Mapping(source = "listDetailEntranceDTO", target = "listDetailEntranceDTO")
    @Mapping(target="personId", ignore = true)
    @Mapping(target="userId", ignore = true)
    fun toDTO(entrance: Entrance, listDetailEntranceDTO: MutableList<DetailEntranceDTO>) : EntranceDTO
}