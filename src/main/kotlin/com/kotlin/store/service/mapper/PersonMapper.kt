package com.kotlin.store.service.mapper

import com.kotlin.store.repository.entity.Person
import com.kotlin.store.service.dto.PersonDTO
import org.mapstruct.Mapper

@Mapper(componentModel = "spring")
interface PersonMapper : EntityMapper<PersonDTO, Person>