package com.kotlin.store.service.mapper

import com.kotlin.store.repository.entity.DetailSale
import com.kotlin.store.repository.entity.Product
import com.kotlin.store.repository.entity.Sale
import com.kotlin.store.service.dto.DetailSaleDTO
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.springframework.stereotype.Service
@Mapper(componentModel = "spring")
interface DetailSaleMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(source = "product", target = "product")
    @Mapping(source = "sale", target = "sale")
    fun toEntity(detailSaleDTO: DetailSaleDTO, sale: Sale, product: Product): DetailSale

    @Mapping(source = "product.id", target = "productId")
    @Mapping(source = "sale.id", target = "saleId")
    fun toDTO(detailSale: DetailSale) : DetailSaleDTO
}
