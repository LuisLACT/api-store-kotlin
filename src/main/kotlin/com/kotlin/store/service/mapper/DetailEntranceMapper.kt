package com.kotlin.store.service.mapper

import com.kotlin.store.repository.entity.DetailEntrance
import com.kotlin.store.repository.entity.Entrance
import com.kotlin.store.repository.entity.Product
import com.kotlin.store.repository.entity.User
import com.kotlin.store.service.dto.DetailEntranceDTO
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.springframework.stereotype.Service

@Mapper(componentModel = "spring", uses = [ProductMapper::class, EntranceMapper::class])
interface DetailEntranceMapper{

    @Mapping(source = "product.id", target = "productId")
    @Mapping(source = "entrance.id", target = "entranceId")
    fun toDTO(entity: DetailEntrance) : DetailEntranceDTO

    @Mapping(target = "id", ignore = true)
    @Mapping(source = "product.id", target = "product.id")
    @Mapping(source = "entrance.id", target = "entrance.id")
    fun toEntity(dto: DetailEntranceDTO, entrance: Entrance, product: Product): DetailEntrance
}
