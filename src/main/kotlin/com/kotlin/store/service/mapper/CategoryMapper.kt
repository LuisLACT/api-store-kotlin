package com.kotlin.store.service.mapper

import com.kotlin.store.repository.entity.Category
import com.kotlin.store.service.dto.CategoryDTO
import org.mapstruct.Mapper

@Mapper(componentModel = "spring", uses = [])
interface CategoryMapper: EntityMapper<CategoryDTO, Category>