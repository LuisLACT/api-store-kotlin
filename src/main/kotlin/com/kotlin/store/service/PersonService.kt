package com.kotlin.store.service

import com.kotlin.store.service.dto.PersonDTO
import com.kotlin.store.repository.entity.Person
import com.kotlin.store.repository.PersonRepository
import com.kotlin.store.service.mapper.PersonMapper
import com.kotlin.store.service.utils.Validations
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

@Service
class PersonService(private val personRepository: PersonRepository, private val personMapper: PersonMapper) {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(PersonService::class.java)
    }
    fun addPerson(personDTO: PersonDTO): PersonDTO {
        logger.debug("Service to save person: {}", personDTO)
        val personEntity = personRepository.save(personMapper.toEntity(personDTO))
        return personMapper.toDTO(personEntity)
    }

    fun updatePerson(personDTO: PersonDTO, personId: Int): PersonDTO {
        logger.debug("Service to update person: {}", personDTO)
        val personEntity = personRepository.findById(personId)
        Validations.validationPerson(personEntity)
        return personEntity.get().let {
            it.personType = personDTO.personType
            it.name = personDTO.name
            it.documentType = personDTO.documentType
            it.documentNumber = personDTO.documentNumber
            it.address = personDTO.address
            it.phone = personDTO.phone
            it.email = personDTO.email
            personRepository.save(it)
            personMapper.toDTO(it)
        }
    }

    fun getAllPerson(pageable: Pageable) : Page<PersonDTO> {
        logger.debug("Service to get all people")
        return personRepository
            .findAll(pageable)
            .map {personMapper.toDTO(it) }
    }

    fun deletePerson(personId: Int) {
        logger.debug("Service to delete person with ID: {}", personId)
        val personEntity = personRepository.findById(personId)
        Validations.validationPerson(personEntity)
        personRepository.deleteById(personId)
    }
}
