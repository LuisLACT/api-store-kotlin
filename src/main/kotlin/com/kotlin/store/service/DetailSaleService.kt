package com.kotlin.store.service

import com.kotlin.store.exceptions.CustomNotFoundException
import com.kotlin.store.repository.DetailSaleRepository
import com.kotlin.store.repository.entity.DetailSale
import com.kotlin.store.repository.entity.Sale
import com.kotlin.store.service.dto.DetailSaleDTO
import com.kotlin.store.service.mapper.DetailSaleMapper
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import kotlin.math.log


@Service
class DetailSaleService(
    val detailSaleRepository: DetailSaleRepository,
    val productService: ProductService,
    val detailSaleMapper: DetailSaleMapper
) {
    companion object {
        val logger: Logger = LoggerFactory.getLogger(DetailSaleService::class.java)
    }
    fun addDetailSaleList(detailSaleListDTO : MutableList<DetailSaleDTO>, sale: Sale) : MutableList<DetailSaleDTO>{
        logger.debug("Service to add detailSaleList by sale")
        val listDetailSaleDTO: MutableList<DetailSaleDTO> = ArrayList()
         detailSaleListDTO.map {detailSaleDTO ->
            val product = productService.updateStockProduct(
                detailSaleDTO.productId?:throw CustomNotFoundException("Product not found"),
                detailSaleDTO.quantity,
                sale.status)
            val detailSale = detailSaleRepository.save(
                detailSaleMapper.toEntity(detailSaleDTO, sale, product))
            listDetailSaleDTO.add(detailSaleMapper.toDTO(detailSale))
         }
        return listDetailSaleDTO
    }

    fun findAllDetailSaleBySaleId(saleId: Int) : List<DetailSale> {
        logger.debug("Service to get all DetailSaleList by Sale ID")
        return detailSaleRepository.findAllBySaleId(saleId)
    }
}
