package com.kotlin.store.service

import com.kotlin.store.exceptions.CustomNotFoundException
import com.kotlin.store.service.dto.ProductFakeDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import javax.annotation.PostConstruct
import org.slf4j.Logger
import org.slf4j.LoggerFactory

@Service
class ProductFakeServiceImpl {

    @Autowired
    private val retrofit: Retrofit? = null

    private var productFakeService: ProductFakeService? = null

    companion object {
        val logger: Logger = LoggerFactory.getLogger(CategoryService::class.java)
    }
    @PostConstruct
    fun setup() {
        productFakeService = retrofit!!.create(ProductFakeService::class.java)
    }

    fun getAllProducts(): List<ProductFakeDto>? {
        logger.debug("Service to get All fake product")
        val call : Call<List<ProductFakeDto>> = productFakeService!!.getAllProducts()!!
        try {
            val response: Response<List<ProductFakeDto>> = call.execute()
            if(response.isSuccessful) {
                logger.debug("Get all fake products response: {} ", response.body())
                return response.body()
            }
        } catch (e: CustomNotFoundException) {
            logger.error("Error happened! $e")
        }
        return  null
    }

    fun saveProductFake(productFakeDto: ProductFakeDto) : ProductFakeDto? {
        logger.debug("Service to save fake product in external API : {}", productFakeDto)
        val call : Call<ProductFakeDto> = productFakeService!!.saveProductFake(productFakeDto)
        try {
            val response: Response<ProductFakeDto> = call.execute()
            if(response.isSuccessful) {
                logger.debug("Save fake products response: {} ", response.body())
                return response.body()
            }
        } catch (e: CustomNotFoundException) {
            logger.error("Error happened! $e")
        }
        return null
    }
}