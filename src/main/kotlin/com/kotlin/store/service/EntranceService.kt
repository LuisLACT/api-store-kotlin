package com.kotlin.store.service

import com.kotlin.store.exceptions.CustomNotFoundException
import com.kotlin.store.service.dto.DetailEntranceDTO
import com.kotlin.store.service.dto.EntranceDTO
import com.kotlin.store.repository.PersonRepository
import com.kotlin.store.repository.UserRepository
import com.kotlin.store.repository.EntranceRepository
import com.kotlin.store.service.mapper.DetailEntranceMapper
import com.kotlin.store.service.mapper.EntranceMapper
import com.kotlin.store.service.utils.Format
import com.kotlin.store.service.utils.Validations
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.stereotype.Service
import org.springframework.data.domain.Pageable
import java.time.LocalDateTime

@Service
class EntranceService(private val detailEntranceService: DetailEntranceService,
    private val personRepository: PersonRepository,
    private val userRepository: UserRepository,
    private val entranceRepository: EntranceRepository,
    private val entranceMapper: EntranceMapper,
    private val detailEntranceMapper: DetailEntranceMapper){

    companion object {
        val logger: Logger = LoggerFactory.getLogger(EntranceService::class.java)
    }
    fun registerEntrance(entranceDTO: EntranceDTO) : EntranceDTO {
        logger.debug("Service to register entrance: {}", entranceDTO)
        val provider = personRepository
            .findById(entranceDTO.personId?: throw CustomNotFoundException("Person not found"))
        Validations.validationPerson(provider)
        val  user = userRepository.findById(entranceDTO.userId?: throw CustomNotFoundException("User not found"))
        Validations.validationUser(user)
        val entranceEntity = entranceMapper.toEntity(entranceDTO, user.get(), provider.get())
        entranceEntity.date = LocalDateTime.now()
        entranceRepository.save(entranceEntity)
        val listDetailEntranceDTO = detailEntranceService
            .addDetailEntranceList(
                entranceDTO.listDetailEntranceDTO?: throw CustomNotFoundException("Entrance not found"),
                entranceEntity)
        return entranceMapper.toDTO(entranceEntity, listDetailEntranceDTO)
    }
    fun getAllEntrance(pageable: Pageable) : Page<EntranceDTO> {
        logger.debug("Service to get all entrance")
        val entranceList = entranceRepository.findAll(pageable)
        val entranceDTOList: MutableList<EntranceDTO> = ArrayList()
        entranceList.map { entrance ->
            val detailEntranceDTOList : MutableList<DetailEntranceDTO> = ArrayList()
            val detailEntranceList = detailEntranceService
                .getAllDetailEntranceByEntranceId(entrance.id?: throw CustomNotFoundException("Entrance not found"))
            detailEntranceList.map { detailEntrance ->
                detailEntranceDTOList.add(detailEntranceMapper.toDTO(detailEntrance))
            }
            val entranceDTO = entranceMapper.toDTO(entrance, detailEntranceDTOList)
            entranceDTO.date = Format.formatDate(entrance.date!!)
            entranceDTOList.add(entranceDTO)
        }
        return PageImpl(entranceDTOList)
    }
}