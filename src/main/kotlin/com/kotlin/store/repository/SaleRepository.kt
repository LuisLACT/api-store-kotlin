package com.kotlin.store.repository

import com.kotlin.store.repository.entity.Sale
import org.springframework.data.jpa.repository.JpaRepository

interface SaleRepository: JpaRepository<Sale, Int>

