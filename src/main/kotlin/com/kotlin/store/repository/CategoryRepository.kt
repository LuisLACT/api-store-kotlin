package com.kotlin.store.repository

import com.kotlin.store.repository.entity.Category
import org.springframework.data.jpa.repository.JpaRepository

interface CategoryRepository : JpaRepository<Category, Int>
