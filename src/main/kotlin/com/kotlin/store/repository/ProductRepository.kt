package com.kotlin.store.repository

import com.kotlin.store.repository.entity.Product
import org.springframework.data.jpa.repository.JpaRepository

interface ProductRepository: JpaRepository<Product, Int>
