package com.kotlin.store.repository

import com.kotlin.store.repository.entity.DetailEntrance
import org.springframework.data.jpa.repository.JpaRepository

interface DetailEntranceRepository: JpaRepository<DetailEntrance, Int> {
    fun findAllByEntranceId(id: Int) : MutableList<DetailEntrance>
}
