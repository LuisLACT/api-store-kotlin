package com.kotlin.store.repository

import com.kotlin.store.repository.entity.DetailSale
import org.springframework.data.jpa.repository.JpaRepository

interface DetailSaleRepository: JpaRepository<DetailSale, Int> {
    fun findAllBySaleId(id: Int): List<DetailSale>


}
