package com.kotlin.store.repository.entity.enumeration

enum class PersonType {
    CLIENT,
    PROVIDER
}
