package com.kotlin.store.repository.entity

import java.math.BigDecimal
import java.time.LocalDateTime
import javax.persistence.Table
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.ManyToOne
import javax.persistence.JoinColumn

@Entity
@Table(name = "tbl_sale")
data class Sale(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    var id: Int?,
    var serie: String,
    var voucherNumber: Int,
    var date: LocalDateTime?,
    var total: BigDecimal,
    var status: Boolean,
    @ManyToOne
    @JoinColumn(name = "client_id", referencedColumnName = "id")
    var person: Person,
    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    var user: User
    )
