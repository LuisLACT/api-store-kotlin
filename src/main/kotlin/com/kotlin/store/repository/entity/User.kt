package com.kotlin.store.repository.entity

import javax.persistence.Table
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.ManyToMany
import javax.persistence.JoinTable
import javax.persistence.JoinColumn

@Entity
@Table(name="tbl_users")
data class User(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    var id: Int?,
    var name: String,
    var password: String,
    var username: String,
    var email: String,
    var status: Boolean,
    @ManyToMany
    @JoinTable(
        name = "user_rol",
        joinColumns = [JoinColumn(name = "user_id", referencedColumnName = "id")],
        inverseJoinColumns = [JoinColumn(name = "rol_name", referencedColumnName = "name")]
    )
    var rol : Set<Rol> = HashSet()
)
