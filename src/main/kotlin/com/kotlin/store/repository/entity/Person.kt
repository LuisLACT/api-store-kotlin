package com.kotlin.store.repository.entity

import com.kotlin.store.repository.entity.enumeration.PersonType
import javax.persistence.Table
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType

@Entity
@Table(name = "tbl_person")
data class Person(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    var id: Int?,
    var personType: PersonType,
    var name: String,
    var documentType: String,
    var documentNumber: Int,
    var address: String,
    var phone: Int,
    var email: String
)
