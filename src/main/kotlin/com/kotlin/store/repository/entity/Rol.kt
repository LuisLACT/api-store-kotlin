package com.kotlin.store.repository.entity

import javax.persistence.Table
import javax.persistence.Entity
import javax.persistence.Id

@Entity
@Table(name = "tbl_rol")
data class Rol (
    @Id
    val name: String
)
