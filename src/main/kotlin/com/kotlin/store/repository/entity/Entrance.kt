package com.kotlin.store.repository.entity

import java.math.BigDecimal
import java.time.LocalDateTime
import javax.persistence.Table
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.ManyToOne
import javax.persistence.JoinColumn

@Entity
@Table(name = "tbl_entrance")
data class Entrance(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    var id: Int?,
    var voucherType: String,
    var voucherSeries: String,
    var voucherNumber: Int,
    var date: LocalDateTime?,
    var total: BigDecimal,
    var status: Boolean,
    @ManyToOne
    @JoinColumn(name = "provider_id", referencedColumnName = "id")
    var person: Person,
    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    var user: User
)
