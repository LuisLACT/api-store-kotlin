package com.kotlin.store.repository.entity

import java.math.BigDecimal
import javax.persistence.Table
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.ManyToOne
import javax.persistence.JoinColumn

@Entity
@Table(name = "tbl_detail_sale")
data class DetailSale(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    var id: Int?,
    var  quantity: BigDecimal,
    var  price: BigDecimal,
    var  discount: BigDecimal,
    @ManyToOne
    @JoinColumn(name = "product_id", referencedColumnName = "id")
    var  product: Product,
    @ManyToOne
    @JoinColumn(name="sale_id", referencedColumnName = "id")
    var sale: Sale
)
