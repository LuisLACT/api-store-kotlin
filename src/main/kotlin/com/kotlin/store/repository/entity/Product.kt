package com.kotlin.store.repository.entity

import java.math.BigDecimal
import javax.persistence.Table
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.ManyToOne
import javax.persistence.JoinColumn
import javax.persistence.Column


@Table(name ="tbl_product")
@Entity
data class Product (
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    var id: Int?,
    var code: String,
    var name: String,
    @Column(name = "price_sale")
    var priceSale: BigDecimal,
    var stock: BigDecimal,
    var description: String,
    var status: Boolean,
    @ManyToOne
    @JoinColumn(name="category_id", referencedColumnName = "id")
    var category: Category
)
