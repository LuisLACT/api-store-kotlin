package com.kotlin.store.repository

import com.kotlin.store.repository.entity.Entrance
import org.springframework.data.jpa.repository.JpaRepository

interface EntranceRepository: JpaRepository<Entrance, Int>
