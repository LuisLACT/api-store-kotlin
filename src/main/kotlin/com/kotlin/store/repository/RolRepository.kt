package com.kotlin.store.repository

import com.kotlin.store.repository.entity.Rol
import org.springframework.data.jpa.repository.JpaRepository

interface RolRepository: JpaRepository<Rol, String>
