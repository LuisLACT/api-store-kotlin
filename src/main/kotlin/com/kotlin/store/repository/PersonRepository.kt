package com.kotlin.store.repository

import com.kotlin.store.repository.entity.Person
import org.springframework.data.jpa.repository.JpaRepository

interface PersonRepository: JpaRepository<Person, Int>
