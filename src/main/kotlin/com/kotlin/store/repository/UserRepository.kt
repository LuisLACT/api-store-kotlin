package com.kotlin.store.repository

import com.kotlin.store.repository.entity.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface UserRepository: JpaRepository<User, Int> {

    /*
    Este metodo ejecuta la siguiente consulta
    select *
    from tbl_users
    where email = email or username = username
     */
    fun findOneByEmailOrUsername(email: String, username: String): Optional<User>


    //CONSULTAS JPQL
    @Query("select u from User u where u.email = :email or u.username = :username")
    fun findUsernameByEmailOrUsernameJPQL(email: String, username: String) : Optional<User>

//    //CONSULTAS NATIVAS
    @Query(value = "select * from tbl_users where email = :email or username = :username", nativeQuery = true)
    fun findUsernameByEmailOrUsernameNative(
        @Param("email") email: String,
        @Param("username") username: String) : Optional<User>
}