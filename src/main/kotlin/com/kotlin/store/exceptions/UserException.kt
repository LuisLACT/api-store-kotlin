package com.kotlin.store.exceptions

import java.lang.RuntimeException

class UserException(message: String): RuntimeException(message)
