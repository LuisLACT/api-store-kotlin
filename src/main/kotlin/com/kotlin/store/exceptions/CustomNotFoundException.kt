package com.kotlin.store.exceptions

import java.lang.RuntimeException

class CustomNotFoundException(message: String) : RuntimeException(message)
