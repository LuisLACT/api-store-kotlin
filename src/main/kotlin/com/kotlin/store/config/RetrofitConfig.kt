package com.kotlin.store.config

import okhttp3.OkHttpClient
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


@Configuration
class RetrofitConfig {

    @Value("\${external.service.base-url}")
    private lateinit var baseUrl: String

    @Bean
    fun retrofit() : Retrofit {
        val httpClient = OkHttpClient.Builder()
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .build()
    }

}