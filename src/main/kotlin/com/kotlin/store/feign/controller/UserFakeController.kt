package com.kotlin.store.feign.controller

import com.kotlin.store.feign.service.dto.UserFakeDto
import com.kotlin.store.feign.service.UserFakeService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class UserFakeController(private val userFakeService: UserFakeService) {

    @GetMapping("/api/user-fake")
    fun getAllUsersFake() : List<UserFakeDto> {
        return userFakeService.getAllUserFake()
    }

}