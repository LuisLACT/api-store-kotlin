package com.kotlin.store.feign.controller

import com.kotlin.store.feign.service.dto.CategoryFakeDto
import com.kotlin.store.feign.service.CategoryFakeService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class CategoryFakeController(private val categoryFakeService: CategoryFakeService) {

    @GetMapping("api/v1/categories")
    fun getCategoriesFake(
        @RequestParam(name = "limit", required = false, defaultValue = "5") limit: Int,
        @RequestParam(name= "import", required = false, defaultValue = "false") import: Boolean
        ) : List<CategoryFakeDto> {
        return categoryFakeService.getCategoriesFake(limit, import)
    }
}