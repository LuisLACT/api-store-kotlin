package com.kotlin.store.feign.service.mapper

import com.kotlin.store.feign.service.dto.CategoryFakeDto
import com.kotlin.store.repository.entity.Category
//import com.kotlin.store.service.dto.CategoryDTO
import org.mapstruct.Mapper
import org.mapstruct.Mapping

@Mapper(componentModel = "spring")
interface CategoryFakeMapper {

    @Mapping(target="id", ignore = true )
    @Mapping(source = "name", target = "name")
    @Mapping(source = "name", target = "description")
    fun toEntity(categoryFakeDto: CategoryFakeDto) : Category
}