package com.kotlin.store.feign.service

import com.kotlin.store.feign.FeignClientExample
import com.kotlin.store.feign.service.dto.UserFakeDto
import com.kotlin.store.service.CategoryService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class UserFakeService(private val feignClientExample: FeignClientExample) {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(UserFakeService::class.java)
    }

    fun getAllUserFake() : List<UserFakeDto> {
        logger.debug("Service to get all fake users")
        return feignClientExample.getUsers()
    }
}