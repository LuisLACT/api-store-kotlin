package com.kotlin.store.feign.service

import com.kotlin.store.feign.service.dto.CategoryFakeDto
import com.kotlin.store.feign.FeignClientExample
import com.kotlin.store.feign.service.mapper.CategoryFakeMapper
import com.kotlin.store.repository.CategoryRepository
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class CategoryFakeService(private val categoryRepository: CategoryRepository,
                          private val feignClientExample: FeignClientExample,
                          private val categoryFakeMapper: CategoryFakeMapper) {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(CategoryFakeService::class.java)
    }
    fun getCategoriesFake(limit: Int, import: Boolean) : List<CategoryFakeDto> {
        logger.debug("Service to get categories fake")
        val categoriesFakeList = feignClientExample.getCategories(limit)
        if(import) {
            logger.debug("Save categories fake")
            categoriesFakeList.map {
                categoryRepository.save(categoryFakeMapper.toEntity(it))
            }
        }
        return categoriesFakeList
    }

}