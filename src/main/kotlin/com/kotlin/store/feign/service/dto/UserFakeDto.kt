package com.kotlin.store.feign.service.dto

data class UserFakeDto(
    val email: String,
    val password: String,
    val name: String,
    val role: String,
    val avatar: String
)
