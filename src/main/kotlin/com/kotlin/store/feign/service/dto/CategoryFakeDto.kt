package com.kotlin.store.feign.service.dto

data class CategoryFakeDto(
    val name: String,
    val image: String
)
