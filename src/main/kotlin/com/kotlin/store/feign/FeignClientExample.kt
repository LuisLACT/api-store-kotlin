package com.kotlin.store.feign

import com.kotlin.store.feign.service.dto.CategoryFakeDto
import com.kotlin.store.feign.service.dto.UserFakeDto
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam

@FeignClient(name = "store-fake-service", url = "https://api.escuelajs.co/")
interface FeignClientExample {

    @GetMapping("api/v1/users")
    fun getUsers() : List<UserFakeDto>

    @GetMapping("api/v1/categories")
    fun getCategories(@RequestParam(
        name = "limit",
        required = false,
        defaultValue = "5") limit: Int) : List<CategoryFakeDto>
}