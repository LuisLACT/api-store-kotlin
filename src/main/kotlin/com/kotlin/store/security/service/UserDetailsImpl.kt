package com.kotlin.store.security.service

import com.kotlin.store.exceptions.CustomNotFoundException
import com.kotlin.store.repository.entity.User
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import java.util.stream.Collectors

class UserDetailsImpl(
    private val id: Int,
//    private val email: String,
    private val username: String,
    private val password: String,
//    private var status: Boolean,
    private val authorities: Collection<GrantedAuthority>
) : UserDetails {

    override fun getAuthorities(): Collection<GrantedAuthority> {
        return authorities
    }

    override fun getPassword(): String {
        return password
    }

    override fun getUsername(): String {
        return username
    }

    override fun isAccountNonExpired(): Boolean {
        return true
    }

    override fun isAccountNonLocked(): Boolean {
        return true
    }

    override fun isCredentialsNonExpired(): Boolean {
        return true
    }

    override fun isEnabled(): Boolean {
        return true
    }

    companion object {
        fun build(user: User): UserDetailsImpl {
            val authorities = user.rol.stream()
                .map { role -> SimpleGrantedAuthority(role.name) }
                .collect(Collectors.toList())
            return UserDetailsImpl(
                user.id?: throw CustomNotFoundException("User not found"),
//                user.email,
                user.username,
                user.password,
//                user.status,
                authorities
            )
        }
    }

}
