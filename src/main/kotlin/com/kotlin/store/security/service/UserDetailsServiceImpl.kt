package com.kotlin.store.security.service

import com.kotlin.store.exceptions.CustomNotFoundException
import com.kotlin.store.exceptions.UserException
import com.kotlin.store.repository.UserRepository
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class UserDetailsServiceImpl(val userRepository: UserRepository): UserDetailsService {

    @Transactional
    override fun loadUserByUsername(username: String?): UserDetails {

        val userEntity =  userRepository
            .findOneByEmailOrUsername(username?: throw CustomNotFoundException("User not found"), username)

        if(!userEntity.isPresent) {
            throw UserException("User not found $username")
        }
        if(!userEntity.get().status) {
            throw UserException("User not found $username")
        }
        return UserDetailsImpl.build(userEntity.get())
    }
}
