package com.kotlin.store.security.jwt

import com.kotlin.store.security.service.UserDetailsImpl
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import io.jsonwebtoken.MalformedJwtException
import io.jsonwebtoken.ExpiredJwtException
import io.jsonwebtoken.UnsupportedJwtException
import io.jsonwebtoken.SignatureException
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Component
import java.util.*

@Component
class JwtUtils {
    companion object {
        val logger = org.slf4j.LoggerFactory.getLogger(JwtUtils::class.java)!!
        const val AUTHORITIES_KEY = "auth"
    }

    @Value("\${spring.jwt.secret}")
    private lateinit var jwtSecret: String

    @Value("\${spring.jwt.expiration}")
    private lateinit var jwtExpiration: String

    fun generateJwtToken(authentication: Authentication ) : String {
        val authorities = authentication.authorities
            .map { it.authority }
            .joinToString { "," }
        val userPrincipal = authentication.principal as UserDetailsImpl
        val map = hashMapOf<String, Any>()
        map[AUTHORITIES_KEY] = authorities
        return Jwts.builder()
            .setSubject(userPrincipal.username)
            .setIssuedAt(Date())
            .setExpiration(Date(Date().time + jwtExpiration.toInt()))
            .signWith(SignatureAlgorithm.HS512, jwtSecret)
            .addClaims(map)
            .compact()

    }

    //Obtenemos el username, anteriormente firmando con la clave secreta que se utilizo para la generacion del token
    fun getUserNameFromJwtToken(token: String) : String {
        return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).body.subject
    }

    fun validateJwtToken(authToken: String ) : Boolean {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken)
            return true
        } catch (e: SignatureException) {
            logger.error("Invalid JWT signature: {} ${e.message}")
        } catch (e: MalformedJwtException) {
            logger.error("Invalid JWT token: {} ${e.message}")
        } catch (e: ExpiredJwtException) {
            logger.error("JWT token is expired: {} ${e.message}")
        } catch (e: UnsupportedJwtException) {
            logger.error("JWT token is unsupported: {} ${e.message}")
        } catch (e: IllegalArgumentException) {
            logger.error("JWT claims string is empty: {} ${e.message}")
        }
        return false
    }
}
