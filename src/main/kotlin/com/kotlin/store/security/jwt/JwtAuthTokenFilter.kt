package com.kotlin.store.security.jwt

import com.kotlin.store.exceptions.UserException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource
import org.springframework.stereotype.Component
import org.springframework.util.StringUtils
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class JwtAuthTokenFilter(
    val jwtUtils: JwtUtils,
    val userDetailsService: UserDetailsService
): OncePerRequestFilter() {

    companion object {
        const val AUTHORIZATION_HEADER = "Authorization"
        const val HEADER =7
    }

    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        filterChain: FilterChain
    ) {
        try {
            val jwt = parseJwt(request)
            if(jwt != null && jwtUtils.validateJwtToken(jwt)) {
                val userName = jwtUtils.getUserNameFromJwtToken(jwt)
                val userDetails = userDetailsService.loadUserByUsername(userName)
                val authentication = UsernamePasswordAuthenticationToken(userDetails, null, userDetails.authorities)
                authentication.details = WebAuthenticationDetailsSource().buildDetails(request)
                // Almacena información sobre la autenticación y la autorización del usuario actual en una solicitud web
                SecurityContextHolder.getContext().authentication =authentication
            }
        } catch (e: UserException) {
            logger.error("Cannot set user authentication: {}", e)
        }
        filterChain.doFilter(request,response)
    }

    private fun parseJwt(request: HttpServletRequest ) : String? {
        val headerAuth = request.getHeader(AUTHORIZATION_HEADER)

        if (StringUtils.hasText(headerAuth) && headerAuth.startsWith("Bearer")) {
            return  headerAuth.substring(HEADER, headerAuth.length)
        }
        return null
    }
}
