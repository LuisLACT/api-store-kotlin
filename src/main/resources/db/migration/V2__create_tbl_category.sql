 CREATE TABLE IF NOT EXISTS tbl_category (
	id int4 NOT NULL,
	description varchar(255) NULL,
	name varchar(255) NULL,
	CONSTRAINT tbl_category_pkey PRIMARY KEY (id)
);