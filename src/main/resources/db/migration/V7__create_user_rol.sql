CREATE TABLE IF NOT EXISTS user_rol (
	user_id int4 NOT NULL REFERENCES tbl_users(id),
	rol_name varchar(255) NOT NULL REFERENCES tbl_rol(name),
	CONSTRAINT user_rol_pkey PRIMARY KEY (user_id, rol_name)
);