CREATE TABLE IF NOT EXISTS tbl_sale (
	id int4 NOT NULL,
	"date" timestamp NULL,
	serie varchar(255) NULL,
	status bool NOT NULL,
	total numeric(19, 2) NULL,
	voucher_number int4 NOT NULL,
	client_id int4 NULL REFERENCES tbl_person(id),
	user_id int4 NULL REFERENCES tbl_users(id),
	CONSTRAINT tbl_sale_pkey PRIMARY KEY (id)
);