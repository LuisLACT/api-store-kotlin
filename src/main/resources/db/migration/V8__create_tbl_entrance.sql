CREATE TABLE IF NOT EXISTS tbl_entrance (
	id int4 NOT NULL,
	date timestamp NULL,
	status bool NULL,
	total numeric(19, 2) NULL,
	voucher_number int4 NOT NULL,
	voucher_series varchar(255) NULL,
	voucher_type varchar(255) NULL,
	provider_id int4 NULL REFERENCES tbl_person(id),
	user_id int4 NULL REFERENCES tbl_users(id),
	CONSTRAINT tbl_entrance_pkey PRIMARY KEY (id)
);