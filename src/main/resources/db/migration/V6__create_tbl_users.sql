CREATE TABLE IF NOT EXISTS tbl_users (
	id int4 NOT NULL,
	email varchar(255) NULL,
	name varchar(255) NULL,
	password varchar(255) NULL,
	status bool NOT NULL,
	username varchar(255) NULL,
	CONSTRAINT tbl_users_pkey PRIMARY KEY (id)
);