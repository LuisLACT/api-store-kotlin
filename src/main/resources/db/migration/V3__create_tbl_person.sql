CREATE TABLE IF NOT EXISTS tbl_person (
	id int4 NOT NULL,
	address varchar(255) NULL,
	document_number int4 NOT NULL,
	document_type varchar(255) NULL,
	email varchar(255) NULL,
	name varchar(255) NULL,
	person_type int4 NULL,
	phone int4 NOT NULL,
	CONSTRAINT tbl_person_pkey PRIMARY KEY (id)
);