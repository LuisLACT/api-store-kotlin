CREATE TABLE IF NOT EXISTS tbl_product (
	id int4 NOT NULL,
	code varchar(255) NULL,
	description varchar(255) NULL,
	name varchar(255) NULL,
	price_sale numeric(19, 2) NULL,
	status bool NOT NULL,
	stock numeric(19, 2) NULL,
	category_id int4 REFERENCES tbl_category(id),
	CONSTRAINT tbl_product_pkey PRIMARY KEY (id)
);