CREATE TABLE IF NOT EXISTS tbl_detail_sale (
	id int4 NOT NULL,
	discount numeric(19, 2) NULL,
	price numeric(19, 2) NULL,
	quantity numeric(19, 2) NULL,
	product_id int4 NULL REFERENCES tbl_product(id),
	sale_id int4 NULL REFERENCES tbl_sale(id),
	CONSTRAINT tbl_detail_sale_pkey PRIMARY KEY (id)
);