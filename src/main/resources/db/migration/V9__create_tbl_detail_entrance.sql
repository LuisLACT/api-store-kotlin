CREATE TABLE public.tbl_detail_entrance (
	id int4 NOT NULL,
	price numeric(19, 2) NULL,
	quantity numeric(19, 2) NULL,
	entrance_id int4 NULL REFERENCES tbl_entrance(id),
	product_id int4 NULL REFERENCES tbl_product(id),
	CONSTRAINT tbl_detail_entrance_pkey PRIMARY KEY (id)
);